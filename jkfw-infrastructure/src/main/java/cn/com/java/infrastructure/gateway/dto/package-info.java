/**
 * 网关数据传输对象
 * <p>
 *     该包下的类主要用于定义网关数据传输对象，如：RequestDTO、ResponseDTO 等。
 *     <br>
 * </p>
 */
package cn.com.java.infrastructure.gateway.dto;