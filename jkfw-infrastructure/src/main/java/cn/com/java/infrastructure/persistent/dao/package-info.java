/**
 * DAO 接口；IXxxDao
 * DAO 接口实现；XxxDaoImpl
 * DAO
 * 1. 数据访问对象，是一个面向对象的数据库接口，它提供了一种抽象机制，使得应用程序可以访问不同的数据库或数据库系统。
 * 2. DAO 的主要目的是将应用程序代码和数据存储细节分离，使得应用程序代码更加清晰，更容易维护。
 * 3. DAO 的设计模式是一种面向对象的设计模式，它是一种数据访问对象模式。
 */
package cn.com.java.infrastructure.persistent.dao;