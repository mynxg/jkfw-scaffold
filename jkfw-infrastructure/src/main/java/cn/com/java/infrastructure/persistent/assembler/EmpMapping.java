package cn.com.java.infrastructure.persistent.assembler;

import cn.com.java.domain.xxx.model.entity.EmpEntity;
import cn.com.java.infrastructure.persistent.po.Emp;
import cn.com.java.types.util.IMapping;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import java.util.List;

/**
 * @description 对象转换配置
 */
@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE,unmappedSourcePolicy = ReportingPolicy.IGNORE)
public interface EmpMapping extends IMapping<EmpEntity, Emp> {

    @Override
    Emp sourceToTarget(EmpEntity var1);

    @Override
    EmpEntity targetToSource(Emp var1);

    @Override
    List<Emp> sourceToTarget(List<EmpEntity> var1);

    @Override
    List<EmpEntity> targetToSource(List<Emp> var1);
}
