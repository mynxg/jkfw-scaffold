/**
 * 网关API
 * <p>
 *     该包下的类主要用于定义网关API，如：GatewayAPI、GatewayRequest、GatewayResponse 等。
 *     <br>
 *     该包下的类主要用于处理网关请求，包括接收请求、解析请求、处理请求、返回响应等操作。
 *     <br>
 * </p>
 */
package cn.com.java.infrastructure.gateway.api;