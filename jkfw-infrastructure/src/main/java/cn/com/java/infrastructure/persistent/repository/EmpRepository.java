package cn.com.java.infrastructure.persistent.repository;

import cn.com.java.domain.xxx.model.entity.EmpEntity;
import cn.com.java.domain.xxx.repository.IEmpRepository;
import cn.com.java.infrastructure.persistent.assembler.EmpMapping;
import cn.com.java.infrastructure.persistent.dao.EmpDao;
import cn.com.java.infrastructure.persistent.po.Emp;
import cn.com.java.types.util.RedisUtils;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nxg
 * @title EmpRepository
 * @createTime 2024/5/28 16:32
 * @description
 */
@Repository
public class EmpRepository implements IEmpRepository {

    @Resource
    private EmpDao empDao;

    //对象转换
    @Resource
    private EmpMapping empMapping;

    @Override
    public List<EmpEntity> selectEmpList() {
        //Mybatis-Plus测试
        List<Emp> emps = empDao.selectEmpList();
        System.out.println(emps);

        //对象转换方式1
        List<EmpEntity> empEntityList = new ArrayList<>();
        for (Emp emp : emps) {
            EmpEntity empEntity = new EmpEntity();
            empEntity.setEmpNo(emp.getEmpNo());
            empEntity.setEName(emp.getEName());
            empEntityList.add(empEntity);
        }

        //对象转换方式2  MapStruct
        List<EmpEntity> empEntityList1 = empMapping.targetToSource(emps);
        System.out.println("MapStruct="+empEntityList1);


        //redis工具类测试
        boolean setResult = RedisUtils.setCacheList("empList", empEntityList);
        List<EmpEntity> empListRedis = RedisUtils.getCacheList("empList");
        System.out.println("添加到redis状态："+setResult+",EmpEntity="+empListRedis);

        return empEntityList;
    }
}
