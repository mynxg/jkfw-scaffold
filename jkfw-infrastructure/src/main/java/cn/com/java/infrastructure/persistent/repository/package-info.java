/**
 * 仓储实现；用于实现 domain 中定义的仓储接口，如；IXxxRepository 在 Repository 中调用服务
 * 以及调用持久化层的接口，如：IXxxMapper
 * <p>
 *     该包下的类主要用于实现 domain 中定义的仓储接口，如：IXxxRepository，在 Repository 中调用服务
 *     以及调用持久化层的接口，如：IXxxMapper
 *     <br>
 * </p>
 */
package cn.com.java.infrastructure.persistent.repository;