package cn.com.java.infrastructure.persistent.po;

import lombok.Data;

/**
 * @author nxg
 * @title Emp
 * @createTime 2024/5/28 16:17
 * @description
 */
@Data
public class Emp {
    private String empNo;
    private String eName;
    private String job;
    private String mgr;
}
