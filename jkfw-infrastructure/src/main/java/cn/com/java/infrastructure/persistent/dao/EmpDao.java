package cn.com.java.infrastructure.persistent.dao;

import cn.com.java.infrastructure.persistent.po.Emp;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @author nxg
 * @title EmpDao
 * @createTime 2024/5/28 16:17
 * @description
 */
@Mapper
public interface EmpDao extends BaseMapper<Emp> {

    List<Emp> selectEmpList();
}
