#  DDD 脚手架

## 项目介绍
DDD 脚手架是一个基于领域驱动设计的脚手架。

## 项目结构
```
jkfw-scaffold
├── jkfw-app -- 启动类
├── jkfw-domain -- 领域模型
├── jkfw-infrastructure -- 基础设施
├── jkfw-triggers -- 触发器
└── jkfw-types -- 公共类型
```


