/**
 * @Author: mynxg 
 * @Date: 2024-08-15 23:34:05
 * @LastEditors: mynxg mynxg@qq.com
 * @LastEditTime: 2024-08-15 23:34:51
 * @FilePath: jkfw-app/src/main/java/cn/com/java/config/SnailJobConfig.java
 * @Description: 这是默认设置, 可以在设置》工具》File Description中进行配置
 */
package cn.com.java.config;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.LoggerContext;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.aizuda.snailjob.client.common.appender.SnailLogbackAppender;
import com.aizuda.snailjob.client.common.event.SnailClientStartingEvent;
//import com.aizuda.snailjob.client.starter.EnableSnailJob;
import com.aizuda.snailjob.client.starter.EnableSnailJob;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.AutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.event.EventListener;
import org.springframework.core.Ordered;
import org.springframework.scheduling.annotation.EnableScheduling;

@AutoConfiguration
@ConditionalOnProperty(prefix = "snail-job", name = "enabled", havingValue = "true")
@EnableScheduling
//@EnableSnailJob(order = Ordered.LOWEST_PRECEDENCE)
@EnableSnailJob
public class SnailJobConfig {
    @EventListener(SnailClientStartingEvent.class)
    public void onStarting(SnailClientStartingEvent event) {
        LoggerContext lc = (LoggerContext) LoggerFactory.getILoggerFactory();
        SnailLogbackAppender<ILoggingEvent> ca = new SnailLogbackAppender<>();
        ca.setName("snail_log_appender");
        ca.start();
        Logger rootLogger = lc.getLogger(Logger.ROOT_LOGGER_NAME);
        rootLogger.addAppender(ca);
    }
}
