package cn.com.java.test;

import cn.com.java.domain.xxx.model.entity.EmpEntity;
import cn.com.java.domain.xxx.repository.IEmpRepository;
import cn.com.java.infrastructure.persistent.dao.EmpDao;
import cn.com.java.infrastructure.persistent.po.Emp;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiTest {

    @Test
    public void test() {
        log.info("测试完成");
    }

    @Resource
    private IEmpRepository empRepository;

    @Test
    public void test01() {

        List<EmpEntity> empEntityList = empRepository.selectEmpList();
        System.out.println(empEntityList);
    }

}
