package cn.com.java.trigger.http;

import cn.com.java.domain.xxx.model.entity.EmpEntity;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

/**
 * @author nxg
 * @title EmpController
 * @createTime 2024/5/31 9:35
 * @description
 */
@Tag(name = "部门管理")
@RestController
@Slf4j
public class EmpController {

    @Operation(summary = "查询")
    @GetMapping("query")
    public EmpEntity query(@Parameter(name = "name", description = "名称") String name) {
        EmpEntity empEntity = new EmpEntity();
        empEntity.setEName(name);
        return empEntity;
    }

    @Operation(summary = "列表")
    @PostMapping("list")
    public List<EmpEntity> list() {
        return new ArrayList<EmpEntity>();
    }

    @Operation(summary ="新增")
    @PostMapping("add")
    public EmpEntity add(EmpEntity userDO) {
        return new EmpEntity();
    }

    @Operation(summary ="修改")
    @PostMapping("update")
    public EmpEntity update(EmpEntity userDO) {
        return new EmpEntity();
    }

    @Operation(summary ="删除")
    @PostMapping("delete")
    public Boolean delete(Integer id) {
        return true;
    }
}
