/**
 * HTTP 接口服务
 * <p>
 *     该包下的类主要用于处理 HTTP 请求，包括接收请求、解析请求、处理请求、返回响应等操作。
 * </p>
 *
 */
package cn.com.java.trigger.http;