/**
 * 任务服务，可以选择使用 Spring 默认提供的 Schedule
 * 或者 Quartz 等定时任务框架，也可以使用分布式任务调度框架，如 Elastic-Job、xxl-job 等。
 *
 */
package cn.com.java.trigger.job;