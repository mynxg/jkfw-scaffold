/**
 * 仓储服务
 * 1. 定义仓储接口，之后由基础设施层做具体实现
 * 2. 仓储接口名称 XxxRepository
 * 3. 仓储接口中定义的方法，一般是对实体的增删改查操作
 * 4. 仓储接口中的方法，一般是对实体的增删改查操作
 */
package cn.com.java.domain.xxx.repository;