package cn.com.java.domain.xxx.repository;

import cn.com.java.domain.xxx.model.entity.EmpEntity;

import java.util.List;

/**
 * @author nxg
 * @title IEmpRepository
 * @createTime 2024/5/28 16:31
 * @description
 */
public interface IEmpRepository {

    List<EmpEntity> selectEmpList();

}
