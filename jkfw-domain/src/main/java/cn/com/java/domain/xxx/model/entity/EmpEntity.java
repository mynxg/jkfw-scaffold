package cn.com.java.domain.xxx.model.entity;

import lombok.Data;

/**
 * @author nxg
 * @title Emp
 * @createTime 2024/5/28 16:33
 * @description
 */
@Data
public class EmpEntity {
    private String empNo;
    private String eName;
    private String job;
    private String mgr;
}
